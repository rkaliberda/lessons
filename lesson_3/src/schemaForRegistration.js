import { validationMethods } from "./validationMethods";

export const validationSchema = {
  username: [
    validationMethods.required("USERNAME_REQUIRED"),
    validationMethods.min(3, "USERNAME_MIN_AMOUNT"),
    validationMethods.max(12, "USERNAME_MAX_AMOUNT"),
  ],
  email: [
    validationMethods.required("EMAIL_REQUIRED"),
    validationMethods.email("EMAIL_WRONG"),
  ],
  password: [
    validationMethods.required("PASSWORD_REQUIRED"),
    validationMethods.min(6, "PASSWORD_MIN_AMOUNT"),
    validationMethods.max(10, "PASSWORD_MAX_AMOUNT"),
  ],
  password2: [
    validationMethods.required("PASSWORD2_REQUIRED"),
    validationMethods.match("password", "password2", "PASSWORD2_MATCH"),
  ],
};
