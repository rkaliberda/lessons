import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";

import common_en from "./locales/en/translation.json";
import common_fr from "./locales/fr/translation.json";
import common_he from "./locales/he/translation.json";

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    debug: true,
    fallbackLng: "en",
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        common: common_en,
      },
      fr: {
        common: common_fr,
      },
      he: {
        common: common_he,
      },
    },
  });

export default i18n;
