import { validationMethods } from "./validationMethods";

export const validationSchema = {
  email: [
    validationMethods.required("EMAIL_REQUIRED"),
    validationMethods.email("EMAIL_WRONG"),
  ],
  password: [validationMethods.required("PASSWORD_REQUIRED")],
};
