import React from "react";

import styled from "styled-components";

import { useTranslation } from "react-i18next";

const HomeWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  h1 {
    color: white;
    margin-top: 15px;
    margin-bottom: 15px;
  }
  p {
    color: white;
  }
`;

const Home = () => {
  const { t, i18n } = useTranslation("common");
  return (
    <>
      <HomeWrapper dir={i18n.dir()}>
        <h1>{t("HOME.TITLE")}</h1>
        <p>{t("HOME.DESCRIPTION")}</p>
      </HomeWrapper>
    </>
  );
};

export default Home;
