import React, { useContext } from "react";
import Form from "../components/Form";
import Input from "../components/Input";
import Button from "../components/Button";

import { MyContext, MyContextProvider } from "../DataContext";

import { createUseStyles } from "react-jss";
import { validationSchema } from "../schemaForLogin";

import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";

const useStyles = createUseStyles({
  myStyleWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    "& h1": {
      color: "white",
      marginTop: "15px",
      marginBottom: "15px",
    },
  },
});

const Login = () => {
  const { values, setValue, errors, buttonHandler } = useContext(MyContext);
  const handleChange = (e) => setValue(e.target.name, e.target.value);
  const navigate = useNavigate();
  const { t, i18n } = useTranslation("common");
  const classes = useStyles();

  return (
    <div dir={i18n.dir()} className={classes.myStyleWrapper}>
      <h1>{t("LOGIN.TITLE")}</h1>
      <Form>
        <Input
          value={values.email}
          text={t("LOGIN.FIELDS.EMAIL")}
          type="email"
          placeholder={t("LOGIN.PLACEHOLDERS.EMAIL")}
          name="email"
          description={errors.email && t(`LOGIN.ERRORS.${errors.email}`)}
          onChange={handleChange}
          error={!!errors.email}
        />
        <Input
          value={values.password}
          text={t("LOGIN.FIELDS.PASSWORD")}
          type="password"
          placeholder={t("LOGIN.PLACEHOLDERS.PASSWORD")}
          name="password"
          description={errors.password && t(`LOGIN.ERRORS.${errors.password}`)}
          onChange={handleChange}
          error={!!errors.password}
        />
      </Form>
      <Button green onClick={buttonHandler}>
        {t("BUTTONS.LOGIN")}
      </Button>
      <Button blue onClick={() => navigate("/registration")}>
        {t("BUTTONS.GO_TO_REG")}
      </Button>
    </div>
  );
};

const LoginContainer = (props) => {
  const initialValues = {
    email: "",
    password: "",
  };
  return (
    <MyContextProvider
      initialValues={initialValues}
      validationSchema={validationSchema}
    >
      <Login {...props} />
    </MyContextProvider>
  );
};

export default LoginContainer;
