import React, { useContext } from "react";
import Form from "../components/Form";
import Input from "../components/Input";
import Button from "../components/Button";

import { MyContext, MyContextProvider } from "../DataContext";

import styled from "styled-components";
import { validationSchema } from "../schemaForRegistration";

import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

const RegistrationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const StyledH1 = styled.h1`
  color: white;
  margin-top: 15px;
  margin-bottom: 15px;
`;
// const h1Wrapper = styled.h1``;

const Registration = () => {
  const { values, setValue, errors, buttonHandler } = useContext(MyContext);
  const handleChange = (e) => setValue(e.target.name, e.target.value);
  const navigate = useNavigate();
  const { t, i18n } = useTranslation("common");
  return (
    <>
      <RegistrationWrapper dir={i18n.dir()}>
        <StyledH1>{t("REGISTRATION.TITLE")}</StyledH1>
        <Form>
          <Input
            value={values.username}
            text={t("REGISTRATION.FIELDS.USERNAME")}
            type="text"
            placeholder={t("REGISTRATION.PLACEHOLDERS.USERNAME")}
            name="username"
            description={
              errors.username && t(`REGISTRATION.ERRORS.${errors.username}`)
            }
            onChange={handleChange}
            error={!!errors.username}
          />
          <Input
            value={values.email}
            text={t("REGISTRATION.FIELDS.EMAIL")}
            type="email"
            placeholder={t("REGISTRATION.PLACEHOLDERS.EMAIL")}
            name="email"
            description={
              errors.email && t(`REGISTRATION.ERRORS.${errors.email}`)
            }
            error={!!errors.email}
            onChange={handleChange}
          />
          <Input
            value={values.password}
            text={t("REGISTRATION.FIELDS.PASSWORD")}
            type="password"
            placeholder={t("REGISTRATION.PLACEHOLDERS.PASSWORD")}
            name="password"
            description={
              errors.password && t(`REGISTRATION.ERRORS.${errors.password}`)
            }
            error={!!errors.password}
            onChange={handleChange}
          />
          <Input
            value={values.password2}
            text={t("REGISTRATION.FIELDS.PASSWORD2")}
            type="password"
            placeholder={t("REGISTRATION.PLACEHOLDERS.PASSWORD2")}
            name="password2"
            description={
              errors.password2 && t(`REGISTRATION.ERRORS.${errors.password2}`)
            }
            onChange={handleChange}
            error={!!errors.password2}
          />
        </Form>
        <Button green onClick={buttonHandler}>
          {t("BUTTONS.REGISTRATION")}
        </Button>
        <Button blue onClick={() => navigate("/login")}>
          {t("BUTTONS.GO_TO_LOGIN")}
        </Button>
      </RegistrationWrapper>
    </>
  );
};

const RegistrationContainer = (props) => {
  const initialValues = {
    username: "",
    email: "",
    password: "",
    password2: "",
  };
  return (
    <MyContextProvider
      initialValues={initialValues}
      validationSchema={validationSchema}
    >
      <Registration {...props} />
    </MyContextProvider>
  );
};

export default RegistrationContainer;
