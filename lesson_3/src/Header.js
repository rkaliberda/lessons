import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Button from "./components/Button";

import { useTranslation } from "react-i18next";

import english from "./assets/img/united_kingdom.png";
import french from "./assets/img/france.png";
import hebrew from "./assets/img/israel.png";

const HeaderWrapper = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: transparent;
  color: white;
  padding: 10px;
  // div {
  //   display: flex;
  //   justify-content: space-between;
  //   align-items: stretch;
  // }
  a {
    color: white;
    text-decoration: none;
    outline: none;
    margin-right: 40px;
    cursor: pointer;
  }

  a:last-child {
    margin-right: 0px;
  }
`;

const lngs = {
  en: { imgPath: english },
  fr: { imgPath: french },
  he: { imgPath: hebrew },
};

const Header = () => {
  const { t, i18n } = useTranslation("common");

  return (
    <>
      <HeaderWrapper dir={i18n.dir()}>
        <div>
          <h1>{t("NAME")}</h1>
        </div>
        <div>
          {Object.keys(lngs).map((lng) => (
            <Button
              key={lng}
              type="submit"
              onClick={() => i18n.changeLanguage(lng)}
              mr
              bxshdw
              transp
            >
              <img src={lngs[lng].imgPath} alt="flag_img" />
            </Button>
          ))}
        </div>
        <div>
          <nav>
            <Link to="/">{t("LINKS.HOME")}</Link>
            <Link to="/login">{t("LINKS.LOGIN")}</Link>
            <Link to="/registration">{t("LINKS.REGISTRATION")}</Link>
          </nav>
        </div>
      </HeaderWrapper>
    </>
  );
};

export default Header;
