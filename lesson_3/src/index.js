import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { createGlobalStyle } from "styled-components";
import "./i18n";
import App from "./App";

const Global = createGlobalStyle`
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
html {
  background: url(https://img.wallpapersafari.com/desktop/1440/900/42/71/bX0ZOH.jpg) no-repeat center center fixed;
  background-size: cover;
  font-family: 'Roboto', sans-serif;
}
`;

ReactDOM.render(
  <>
    <Global />
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </>,
  document.getElementById("root")
);
