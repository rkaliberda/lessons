# Lesson 2-3 React App

Study work with React, Context, React-router. Translate with i18n , style with JSS and styled componets.

## Launch app

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.
