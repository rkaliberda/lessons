import React, { useState } from "react";

export const MyContext = React.createContext({});

const Provider = ({ children, initialValues = {} }) => {
  const [values, setValues] = useState(initialValues);

  const setValue = (name, value) => {
    setValues({ ...values, [name]: value });
  };
  console.log(values);
  return (
    <MyContext.Provider value={{ values, setValues, setValue }}>
      {children}
    </MyContext.Provider>
  );
};

export const MyContextProvider = Provider;
