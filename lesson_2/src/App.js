import React from "react";
import "./App.css";
import { Button } from "./components/Button/Button";
import { Page } from "./components/Page/Page";

export default function App() {
  return (
    <>
      <h3>Registration</h3>
      <Page />
      <Button text="Submit" />
    </>
  );
}
