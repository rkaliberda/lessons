import React, { useContext } from "react";
import "./Form.css";
import { Input } from "../Input/Input";
import { MyContext } from "../../context";

export function Form() {
  const { setValue, values } = useContext(MyContext);
  const onChange = (e) => {
    setValue(e.target.name, e.target.value);
  };
  return (
    <>
      <Input
        fieldName="Username:"
        type="text"
        placeholder="Enter Username"
        required
        onChange={onChange}
        value={values.username}
        name="username"
      />
      <Input
        fieldName="First name:"
        type="text"
        placeholder="Enter your first name"
        required
        onChange={onChange}
        value={values.firstName}
        name="firstName"
      />
      <Input
        fieldName="Age:"
        type="number"
        placeholder="Enter your age"
        required
        onChange={onChange}
        value={values.age}
        name="age"
      />
      <Input
        fieldName="Password:"
        type="password"
        placeholder="Enter your password"
        required
        onChange={onChange}
        value={values.password}
        name="password"
      />
    </>
  );
}
