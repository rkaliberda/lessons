import React from "react";
import "./Button.css";

export function Button({ text }) {
  return <button>{text}</button>;
}
