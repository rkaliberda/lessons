import React from "react";
import "./Input.css";

export function Input({ fieldName, type, placeholder, ...props }) {
  return (
    <div>
      <p>{fieldName}</p>
      <input type={type} placeholder={placeholder} required {...props} />
    </div>
  );
}
