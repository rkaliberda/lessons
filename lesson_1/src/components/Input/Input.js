import React from "react";
import "./Input.css";

export class Input extends React.Component {
  render() {
    return (
      <input
        className={this.props.className}
        type={this.props.type}
        placeholder={this.props.placeholder}
        name={this.props.name}
        required
      />
    );
  }
}
